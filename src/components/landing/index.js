import React from 'react';
import Button from 'react-md/lib/Buttons/Button';

import sass from './index.scss';

const Landing = () => (
  <div className={sass.landing}>
    <h1 className="md-display-3">اسکالا</h1>
    <Button
      raised
      primary
      href="https://accounts.google.com/o/oauth2/auth?scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.email&client_id=668531569847-6fu5bilrdgn0k977tfppho5s45b4dfp9.apps.googleusercontent.com&redirect_uri=http%3A%2F%2Flocalhost%3A4050%2Fauth%2Fgoogle%2Fcallback&response_type=code">ورود</Button>
  </div>
);

export default Landing;
