import React, { Component } from 'react';
import { graphql } from 'react-apollo';
import LinearProgress from 'react-md/lib/Progress/LinearProgress';

import Header from 'src/components/dashboard/header';
import AddSurveyButton from 'src/components/dashboard/addSurveyButton';
import query from 'src/graphql/queries/currentUser.gql';

export default WrappedComponent => {
  @graphql(query)
  class RequireAuth extends Component {
    componentWillUpdate(nextProps) {
      if (!nextProps.data.loading && !nextProps.data.currentUser) {
        nextProps.history.push('/');
      }
    }

    render() {
      if (this.props.data.loading) {
        return <LinearProgress id="query-indeterminate-progress" />;
      }
      const { currentUser } = this.props.data;
      return (
        <div>
          <Header currentUser={currentUser} />
          <WrappedComponent {...this.props} />
          <AddSurveyButton />
        </div>
      );
    }
  }

  return RequireAuth;
};
