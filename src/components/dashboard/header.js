import React from 'react';
import PropTypes from 'prop-types';
import Toolbar from 'react-md/lib/Toolbars';
import Button from 'react-md/lib/Buttons/Button';
import Avatar from 'react-md/lib/Avatars';

const Header = ({ currentUser }) => (
  <Toolbar
    colored
    actions={
      <Avatar
        src={currentUser.picture}
        role="presentation"
        alt={currentUser.email}
        title={currentUser.email} />}
    nav={<Button icon>menu</Button>}
    title="Escala" />
);

Header.propTypes = {
  currentUser: PropTypes.shape({
    picture: PropTypes.string,
    email: PropTypes.string.isRequired,
  }).isRequired,
};

export default Header;
