import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { graphql } from 'react-apollo';
import Button from 'react-md/lib/Buttons/';
import { Card, CardTitle, CardActions } from 'react-md/lib/Cards/';
import LinearProgress from 'react-md/lib/Progress/LinearProgress';
import { Motion, spring } from 'react-motion';

import query from 'src/graphql/queries/survey.gql';

@graphql(query)
class Dashboard extends Component {
  render() {
    const { data } = this.props;
    if (data.loading) {
      return (
        <LinearProgress id="query-indeterminate-progress" />
      );
    }
    if (data.error) {
      return null;
    }
    return (
      <div className="md-grid">
        {data.surveys.map(survey => (
          <Motion
            key={survey.id}
            defaultStyle={{ opacity: 0, marginTop: 100 }}
            style={{ x: spring(1), marginTop: spring(0) }}>
            { ({ marginTop, opacity }) => (
              <div className="md-cell md-cell--3" style={{ marginTop: `${marginTop}px`, opacity }}>
                <Card>
                  <CardTitle title={survey.name} />
                  <CardActions className="md-divider-border md-divider-border--top">
                    <Button flat secondary>ویرایش</Button>
                  </CardActions>
                </Card>
              </div>
            )}
          </Motion>
        ))}
      </div>
    );
  }
}

Dashboard.propTypes = {
  data: PropTypes.shape({
    surveys: PropTypes.shape({
      name: PropTypes.string,
    }),
  }).isRequired,
};

export default Dashboard;
