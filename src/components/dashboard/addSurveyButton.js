import React, { Component } from 'react';
import { graphql } from 'react-apollo';
import Button from 'react-md/lib/Buttons';
import DialogContainer from 'react-md/lib/Dialogs';
import TextField from 'react-md/lib/TextFields';

import mutation from 'src/graphql/mutations/createSurvey.gql';
import query from 'src/graphql/queries/survey.gql';

@graphql(mutation)
class AddSurveyButton extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      name: '',
      error: false,
      errorText: '',
    };
  }

  submit = () => {
    this.props.mutate({
      variables: { name: this.state.name },
      refetchQueries: [{ query }],
    })
      .then(() => this.hide())
      .catch(e => {
        const msg = e.graphQLErrors[0].message;
        this.setState({ error: true, errorText: msg });
      });
  }

  show = () => {
    this.setState({ visible: true });
  };

  hide = () => {
    this.setState({ visible: false });
  };

  render() {
    const { error, errorText, visible } = this.state;
    const actions = [];
    actions.push({
      secondary: true,
      children: 'انصراف',
      onClick: this.hide,
    });
    actions.push({
      primary: true,
      flat: true,
      children: 'ایجاد',
      onClick: this.submit,
    });

    return (
      <div>
        <Button
          floating
          secondary
          onClick={this.show}
          tooltipLabel="ایجاد پرسشنامه"
          tooltipPosition="top"
          className="md-btn--fixed md-btn--fixed-br">
          add
        </Button>
        <DialogContainer
          id="dialog"
          visible={visible}
          onHide={this.hide}
          actions={actions}
          title={this.state.name ? `${this.state.name}` : 'پرسشنامه جدید'}>
          <TextField
            id="name"
            name="name"
            type="text"
            label="نام پرسشنامه"
            helpText="نام برای پرسشنامه"
            helpOnFocus
            error={error}
            errorText={errorText}
            onChange={name => this.setState({ name })}
            required />
        </DialogContainer>
      </div>
    );
  }
}

export default AddSurveyButton;
