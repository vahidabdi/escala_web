// Demonstrates several components on one page, each with their own `export`.
//
// These are smaller components that <Main> imports, and changes depending
// on the page route (via React Router).
//
// <WhenNotFound> demonstrates the use of <NotFound>, a ReactQL helper
// component that signals to our web server that we have a 404 error, to handle
// accordingly

// ----------------------
// IMPORTS

/* NPM */

// React
import React from 'react';

/* ReactQL */

// NotFound 404 handler for unknown routes
import { NotFound } from 'kit/lib/routing';

// ----------------------

// Create a route that will be displayed when the code isn't found
const WhenNotFound = () => (
  <NotFound>
    <h1>صفحه مورد نظر یافت نشد!</h1>
  </NotFound>
);

export default WhenNotFound;
